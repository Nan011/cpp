
/* C program for Merge Sort */
#include<stdlib.h>
#include<stdio.h>
#include<iostream>
#include<chrono>
#include<fstream>
// #include<mpi.h>

using namespace std;

// Merges two subarrays of arr[].
// First subarray is arr[l..m]
// Second subarray is arr[m+1..r]
void merge(double arr[], int l, int m, int r)
{
    int i, j, k;
    int n1 = m - l + 1;
    int n2 =  r - m;

    /* create temp arrays */
    int L[n1], R[n2];

    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1+ j];

    /* Merge the temp arrays back into arr[l..r]*/
    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = l; // Initial index of merged subarray
    while (i < n1 && j < n2)
    {
        if (L[i] <= R[j])
        {
            arr[k] = L[i];
            i++;
        }
        else
        {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    /* Copy the remaining elements of L[], if there
       are any */
    while (i < n1)
    {
        arr[k] = L[i];
        i++;
        k++;
    }

    /* Copy the remaining elements of R[], if there
       are any */
    while (j < n2)
    {
        arr[k] = R[j];
        j++;
        k++;
    }
}

/* l is for left index and r is right index of the
   sub-array of arr to be sorted */
void mergeSort(double arr[], int l, int r)
{
    if (l < r)
    {
		int m=l + (r-l)/2;

        // Serial
        mergeSort(arr, l, m);
        mergeSort(arr, m+1, r);
        merge(arr, l, m, r);

        // Parallel
		// #pragma omp parallel sections
		// {
		// 	#pragma omp section
		// 	{
		// 		mergeSort(arr, l, m); //call 1
		// 	}
		// 	#pragma omp section
		// 	{
		// 		mergeSort(arr, m+1, r); //call 2
		// 	}
		// }
		// merge(arr, l, m, r); //this function is outside the parallel sections since it merges and access the same data 
    }
}

/* UTILITY FUNCTIONS */
/* Function to print an array */
void printArray(double A[], int size)
{
    int i;
    for (i=0;i < size; i++)
        printf("%f ",A[i]);
    printf("\n");
}


/* Driver program to test above functions */
int main()
{
    int arr_size = 16384; //16384
    double arr[arr_size];
    double input;

    ifstream myFile;
    myFile.open("input.csv");
    if (myFile.fail()) {
        cout << "anjing gk bisa"<< endl;
    }

    double number;
    int index = 0;
    while (myFile.good() && index < arr_size) {
        string line;
        getline(myFile, line, ',');
        number = atof(line.c_str());
        arr[index] = number;
        cout << line << endl;
        cout << index << endl;
        index++;
    }
    myFile.close();

    // for (int i = 0; i < arr_size; i++) {
    //         cin >> input;
    //         arr[i] = input;
    // }

    auto startClock = std::chrono::steady_clock::now();

    mergeSort(arr, 0, arr_size - 1);

    auto endClock = std::chrono::steady_clock::now();

    double getTotalTime = double(std::chrono::duration_cast <std::chrono::nanoseconds>(endClock - startClock).count());

    //Display Execution Time



    printf("\nSorted array is \n");
    printArray(arr, arr_size);
    cout << "Total Execution Time: " << getTotalTime / 1e9 << endl;
    // int threads = omp_get_thread_num();
    return 0;
}
